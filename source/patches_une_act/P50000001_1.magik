#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto:  Migraci�n de Smallworld 4.1 a 4.3
##	Archivo: P50000001_1.magik
##	Contenido: Parche para cambio en la anotaci�n del objeto sheath
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n:25-08-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
sw!patch_software(:mit_fiber_datamodel, 1)
$

sw!patch_requires(50000001, "1", 138036, "1")
$

#Tomado de patch P138036_1.magik
_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: make_annotation()
##	Descripci�n: M�todo sobrecargado para cambiar las anotaciones para el objeto sheath
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para sheath
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para sheath
##	Condiciones: Referirse a la documentaci�n de clases de PNI para sheath
##
##**********************************************************************************************************************************
_method sheath.make_annotation()
        ##
	## Description:
	##  Creates annotation if the annotation option
        ##  :place_annotation? stored on mit_manager is TRUE, otherwise
        ##  does nothing.
	##
        ## Prerequisites:
        ##  The route must be set in order that a suitable
        ##  position can be established for the annotation

	_global mit_manager
	
	_if _not mit_manager.get_option( :place_annotation? ) _orif
	    (geom << _self.route) _is _unset
	_then
		_return
	_endif 

	_local l_geom_world << geom.world

        _local world_units << l_geom_world.world_units
        _local offset << _self.annotation_offset.value_in(world_units)
        _local radius << _self.annotation_radius.value_in(world_units)
	_local offset_multiplier << 1.2 # add some space between annotations for clarity

        mid << geom.line_length/2.0

        _if (mid + offset) > ( 2 * mid )
        _then
                offset << mid
        _endif

        fp << geom.coordinate_for_distance(mid - offset)
        sp << geom.coordinate_for_distance(mid + offset)
        mp << geom.coordinate_for_distance(mid)
        s  << sector_rope.new_with(sector.new_with(fp, sp))

        orient << s.segment_theta(1,2)
        ori    << orient.adjust_ori_between( -float.half_pi, float.half_pi)

        c << coordinate.new_with_polar(radius, ori) + mp

        s << sector_rope.new_with(sector.new_with(mp, c))

		#Inicio de los cambios Autana CT
        sz << _self.annotation_offset_size(:annotation_1, l_geom_world)*offset_multiplier 

		_self.create_annotation(:annotation_1, s.offset(sz), 53, ori, l_geom_world)

		#Fin de cambios Autana CT
		_endmethod
$

sw!declare_patch(50000001, "1", "Cambios para etiquetas en sheath")
$