#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto:  Migraci�n de Smallworld 4.1 a 4.3
##	Archivo: underground_route_ext.magik
##	Contenido: Extensiones para el manejo del objeto underground_route
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 03-10-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw

_pragma(classify_level=restricted, topic={underground_route}, usage={external})
##**********************************************************************************************************************************
##	M�todo: annotation_2_text
##	Descripci�n: M�todo para generar el texto para la etiqueta n�mero 2
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: la etiqueta
##	Condiciones:  Ninguna
##
##**********************************************************************************************************************************
_method underground_route.annotation_2_text
	#N�mero de ductos
	>>  _self.message(:numero_ductos, _unset, _self.conduits.size)
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: numero_ductos
##	Descripci�n: M�todo que retorna el n�mero de ductos contenidos
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: el n�mero de ductos
##	Condiciones:  Ninguna
##
##**********************************************************************************************************************************
_method underground_route.numero_ductos
	>> _self.conduits.size
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: fire_pre_insert_actions()
##	Descripci�n: M�todo sobrecargado para tomar acciones adicionales antes de insertar un registro
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para underground_route
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para underground_route
##	Condiciones: Referirse a la documentaci�n de clases de PNI para underground_route
##
##**********************************************************************************************************************************
_method underground_route.fire_pre_insert_actions(proposed_values) 
	proposed_values[:underground_route_type] << _self.obtener_tipo_ruta(proposed_values[:canalizacion_type])
	>> proposed_values
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: fire_pre_update_actions()
##	Descripci�n: M�todo sobrecargado para tomar acciones adicionales antes de actualizar un registro
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para underground_route
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para underground_route
##	Condiciones: Referirse a la documentaci�n de clases de PNI para underground_route
##
##**********************************************************************************************************************************
_method underground_route.fire_pre_update_actions(proposed_values)
	tipo_canalizacion << proposed_values[:canalizacion_type]
	_if tipo_canalizacion _isnt _unset _then
		proposed_values[:underground_route_type] << _self.obtener_tipo_ruta(tipo_canalizacion)
	_endif
	>> proposed_values
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: obtener_tipo_ruta()
##	Descripci�n: M�todo para la obtener el tipo de ruta seg�n el tipo de canalizaci�n
##	Par�metros de entrada:  tipo de canalizaci�n
##    Par�metros de salida: el tipo de ruta seg�n el tipo de canalizaci�n
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method underground_route.obtener_tipo_ruta(tipo_canalizacion)
	#Valores posibles para el tipo de ruta seg�n el tipo de canalizaci�n
	para_bore << {dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A EDIFICIO|), 
				dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A MURO|),
				dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A POSTE|), 
				dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN FOSO|)}
	para_trench << {dd_dynamic_enumerator.message_for(:canalizacion_type ,:|CUADRADA|), 
				 dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN CARCAMO|)}
	para_unknown << {dd_dynamic_enumerator.message_for(:canalizacion_type ,:|DESCONOCIDO|), 
				  dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN PASO SUBTER|), 
				  dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO ENTRE CAMARAS|)}
	
	#Tipos de ruta
	bore << dd_dynamic_enumerator.message_for(:under_ground_route_type ,:|Bore|)
	trench << dd_dynamic_enumerator.message_for(:under_ground_route_type ,:|Trench|)
	unknown << dd_dynamic_enumerator.message_for(:under_ground_route_type ,:|Unknown|)
	
	#Tipo de ruta
	tipo_ruta << 	_if para_bore.includes_by_equality?(tipo_canalizacion) _then
					>> bore
				_elif para_trench.includes_by_equality?(tipo_canalizacion) _then
					>> trench
				_elif para_unknown.includes_by_equality?(tipo_canalizacion) _then
					>> unknown
				_endif
				
	>> tipo_ruta
_endmethod 
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: estilos_type_location()
##	Descripci�n: M�todo para obtener el tipo de estilo a presentar para el type_location
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: el n�mero del estilo
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method underground_route.estilos_type_location()
	#Se toman el tipo de canalizaci�n y el estado de construcci�n
	tipo_canalizacion << _self.canalizacion_type
	estado << _self.construction_status
	
	#Tipos de canalizaci�n para barras dobles
	tipos_dobles << {dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A EDIFICIO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A POSTE|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|DESCONOCIDO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN FOSO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN PASO SUBTER|)
				}
	
	#Estados para barras naranjas
	estados_naranja << {dd_dynamic_enumerator.message_for(:construction_status_type_type ,:|Removed|),
					 dd_dynamic_enumerator.message_for(:construction_status_type_type ,:|In Service|)}
	
	#Se devuelve el valor seg�n la combinaci�n de la canalizaci�n y el estado
	_if (tipos_dobles.includes_by_equality?(tipo_canalizacion)) _then
		_if estados_naranja.includes_by_equality?(estado) _then
				retorno << 1
		_else
				retorno << 0
		_endif
	_elif tipo_canalizacion = dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A MURO|) _then
		_if estados_naranja.includes_by_equality?(estado) _then
				retorno << 3
		_else
				retorno << 2
		_endif
	 _endif		
	 
	_return retorno	
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: clear_type_location()
##	Descripci�n: M�todo para quitar la geometr�a type_location
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method underground_route.clear_type_location()
    _self.unset_geometry(:type_location)
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: set_type_location()
##	Descripci�n: M�todo para agregar la geometr�a type_location
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method underground_route.set_type_location()
	#Tipos de canalizaci�n v�lidas
	tipos_canalizacion << {dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A EDIFICIO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A POSTE|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|DESCONOCIDO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN FOSO|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|TRAMO EN PASO SUBTER|),
					dd_dynamic_enumerator.message_for(:canalizacion_type ,:|ACOMETIDA A MURO|)}

	#Si el UR tiene canalizaci�n v�lida
	_if (tipos_canalizacion.includes_by_equality?(_self.canalizacion_type)) _then
		#Ruta
		route << _self.route
		
		#Si la ruta es vac�a termina
		_if route _is _unset _then
			_return
		_endif
		
		#Se obtiene la longitud de la ruta
		len << route.line_length
		#Se toma la coordenada
		coord << route.coordinate_for_distance(len)
		#Se crea el sector ropo
		sr << sector_rope.new_with(sector.new_with(coord, route.last_coord))
		#Se limpia la geometr�a anterior
		_self.clear_type_location()
		#Se asigna la geometr�a actual
		type_geom <<_self.make_geometry(:type_location, sr)
		#Se coloca la orientaci�n
		type_geom.orientation << _self.route.end_direction
	_endif 
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: fire_post_update_actions()
##	Descripci�n: M�todo sobrecargado para manejo de type_locacion despu�s de actualizar un registro
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para underground_route
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para underground_route
##	Condiciones: Referirse a la documentaci�n de clases de PNI para underground_route
##
##**********************************************************************************************************************************
_method underground_route.fire_post_update_actions( old_rec )
	#Si cambia el tipo de canalizaci�n se limpia el type_location y se crea de nuevo
	_if _self.canalizacion_type <> old_rec.canalizacion_type
	_then 
		_self.clear_type_location()
		_self.set_type_location()
	_endif
	
	>> _self 
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: estilos_anno_2()
##	Descripci�n: M�todo para obtener el tipo de estilo a presentar para la annotation_2
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: el n�mero del estilo
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method underground_route.estilos_anno_2()
	#Se toma  el estado de construcci�n
	estado << _self.construction_status
	
	#Estados para valor 1
	estados_uno << {dd_dynamic_enumerator.message_for(:construction_status_type_type ,:|Removed|),
				  dd_dynamic_enumerator.message_for(:construction_status_type_type ,:|In Service|)}
	
	#Se devuelve el valor seg�n el estado
	retorno << _if estados_uno.includes_by_equality?(estado) _then
				>> 1
			_else
				>> 0
			_endif
	 
	_return retorno	
_endmethod
$