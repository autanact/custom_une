#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto:  Migraci�n de Smallworld 4.1 a 4.3
##	Archivo: terminator_mods.magik
##	Contenido: Extensiones para el manejo de estilos, etiquetas y campos para el objeto terminator
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 15-07-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw

_pragma(classify_level=basic, topic={if_terminator})
##**********************************************************************************************************************************
##	M�todo: annotation_1_text
##	Descripci�n: M�todo para retornar la etiqueta 1
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: la etiqueta
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method terminator.annotation_1_text
	>> _self.valor_en_alta.default(0.0).as_fixed_string(10,2).trim_spaces()
_endmethod
$

_pragma(classify_level=basic, topic={if_terminator})
##**********************************************************************************************************************************
##	M�todo: annotation_2_text
##	Descripci�n: M�todo para retornar la etiqueta 2
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: la etiqueta
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method terminator.annotation_2_text
	>> _self.valor_en_baja.default(0.0).as_fixed_string(10,2).trim_spaces()
_endmethod
$

_pragma(classify_level=basic, topic={if_terminator})
##**********************************************************************************************************************************
##	M�todo: clear_annotation()
##	Descripci�n: M�todo para retirar las etiquetas
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method terminator.clear_annotation()
    _if _self.annotation_1 _isnt _unset
    _then
        _self.unset_geometry(:annotation_1, _true)
    _endif

    _if _self.annotation_2 _isnt _unset
    _then
        _self.unset_geometry(:annotation_2, _true)
    _endif
_endmethod
$

_pragma(classify_level=basic, topic={if_terminator})
##**********************************************************************************************************************************
##	M�todo: make_annotation()
##	Descripci�n: M�todo para generar la anotaci�n 
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method terminator.make_annotation()

	_global mit_manager

        _if _not mit_manager.get_option( :place_annotation? ) _orif
	    ( geom << _self.location ) _is _unset                 
        _then
                _return
        _endif

	c  << geom.coord

	h_dir    << geom.x_vector
	v_dir    << geom.y_vector
	sym_size << _self.symbol_size(geom)
	s_wid    << sym_size[1]
	s_ht     << sym_size[2]

	long_txt  << 0.715 * s_wid
	alt_txt << 0.25 * s_ht
	
	c1 << c + long_txt * h_dir + alt_txt * v_dir	
	c2 << c + long_txt * h_dir - alt_txt * v_dir	
    
	sr1 << sector_rope.new_with(sector.new_with(c1))
    sr2 << sector_rope.new_with(sector.new_with(c2))	     

    _if (anno1 << _self.annotation_1) _isnt _unset
    _then
        _self.unset_geometry(:annotation_1, _true)
		
    _endif
      
	anno1 << _self.make_geometry(:annotation_1, sr1, _self.annotation_1_text)
	
    _if (anno2 << _self.annotation_2) _isnt _unset
    _then
        _self.unset_geometry(:annotation_2, _true)
    _endif
      
	anno2 << _self.make_geometry(:annotation_2, sr2, _self.annotation_2_text)
      
	anno1.just << 33
	anno2.just << 33
	anno1.orientation << geom.orientation
	anno2.orientation << geom.orientation

_endmethod
$

_pragma(classify_level=basic, topic={if_terminator})
##**********************************************************************************************************************************
##	M�todo: estilos_terminador()
##	Descripci�n: M�todo para uso de estilos
##	Par�metros de entrada:  Ninguno
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method terminator.estilos_terminador()
	_if _self.senal_type = "SI" _then	
		valor << 0
	_else
		valor << 1
	_endif
	
	_return valor

_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: validaciones_y_ajustes()
##	Descripci�n: M�todo para realizar validaciones y ajustes a campos
##	Par�metros de entrada:  lista de propiedades con los valores
##    Par�metros de salida: lista de propiedades con los valores
##	Condiciones:  Ninguna
##
##**********************************************************************************************************************************
_private _method terminator.validaciones_y_ajustes(proposed_values)
	#Retiro de campos en blanco
	retirar_espacios_blancos(proposed_values, :referencia_dir)
	retirar_espacios_blancos(proposed_values, :observacion)
	>> proposed_values
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: fire_pre_insert_actions()
##	Descripci�n: M�todo sobrecargado para tomar acciones adicionales antes de ingresar un registro
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para terminator
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para terminator
##	Condiciones: Referirse a la documentaci�n de clases de PNI para terminator
##
##**********************************************************************************************************************************
_method terminator.fire_pre_insert_actions(proposed_values)
	#Validaciones y ajustes de los campos con espacios
	>> _self.validaciones_y_ajustes(proposed_values)
_endmethod
$

_pragma(classify_level=restricted)
##**********************************************************************************************************************************
##	M�todo: fire_pre_update_actions()
##	Descripci�n: M�todo sobrecargado para tomar acciones adicionales antes de actualizar un registro
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de PNI para terminator
##    Par�metros de salida: Referirse a la documentaci�n de clases de PNI para terminator
##	Condiciones: Referirse a la documentaci�n de clases de PNI para terminator
##
##**********************************************************************************************************************************
_method terminator.fire_pre_update_actions(proposed_values)
	#Validaciones y ajustes de los campos con espacios
	>> _self.validaciones_y_ajustes(proposed_values)
_endmethod
$