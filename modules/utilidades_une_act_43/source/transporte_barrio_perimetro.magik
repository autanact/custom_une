##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto:  
##	Archivo: transporte_barrio_perimetro.magik
##	Contenido: Clase para transporte de elementos perimetro urbano y barrio entre dataset gis y ds_cartografia
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 04-06-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw

_pragma(classify_level=basic, topic={transporte})
##Definici�n de la clase
def_slotted_exemplar(:transportar_elementos, {})
$

_pragma(classify_level=basic, topic={transporte})
##**********************************************************************************************************************************
##	M�todo: ejecutar
##	Descripci�n: Inicia el proceso de transporte de elementos entre dataset gis y ds_cartografia
##	Par�metros de entrada: 	el tipo de elemento a transportar,  valores v�lidos: :perimetro_urbano � :barrio
##						arreglo de c�digos de municipios
##   	Par�metros de salida: 	Ninguno
##	Condiciones: Ninguno
##
##**********************************************************************************************************************************
_method transportar_elementos.ejecutar(tipo_elemento, municipios)
	#Se verifican las condiciones de los par�metor iniciales
	_if (_self.validar_entradas(tipo_elemento, municipios)) _then 
		#Inicializaci�n de datasets
		carto << gis_program_manager.cached_dataset(:ds_cartografia)
		gis << gis_program_manager.cached_dataset(:gis)
		
		_dynamic !current_dsview! << carto
		_dynamic !current_world! << !current_dsview!.world
		
		#Tabla de municipios
		tabla_municipios << carto.collection(:ds_carto_municipio)
		
		#Area total en la cual se van a buscar los elementos
		area_a_cubrir << _unset
		
		#Municipios a usar
		int!municipios << rope.new()
		
		#Para cada id de municipio suministrado
		_for municipio_id _over municipios.fast_elements()
		_loop
			#Se valida la existencia del municipio, si no existe se termina la ejecuci�n
			_if (municipio << tabla_municipios.select(predicate.eq(:codigo_municipio, municipio_id.write_string)).an_element()) _is _unset _then
				write(_self.message(:municipio_no_encontrado,municipio_id))
				_return
			_endif
			int!municipios.add(municipio)
		_endloop
	
		write(_self.message(:iniciando_proceso))
		
		#Inicializaci�n de variables a utilizar en el proceso de transporte
		tabla_limite << tabla_destino << limites << campo_boundary << para_checkpoint << _unset
		
		#Asignaci�n de valores acorde al tipo de elemento a transportar
		_if tipo_elemento = :perimetro_urbano _then
			#Tabla de l�mites pol�ticos de tipo Perimetro Urbano
			tabla_limite << gis.collection(:pol_boundary).select(predicate.eq(:type, dd_dynamic_enumerator.message_for(:political_bnd_type, :|PERIMETRO URBANO|)))
			#Tabla per�metro urbano
			tabla_destino << carto.collection(:ds_carto_perimetro_urbano)
			#Campo geom�trico a utilizar
			campo_boundary << :boundary_city
			#Nombre para el checkpoint
			para_checkpoint << _self.message(:perimetros_urbanos)		
		_else
			#Tabla de l�mites administrativos de tipo Barrio
			tabla_limite << gis.collection(:limite_administrativo).select(predicate.eq(:type, dd_dynamic_enumerator.message_for(:limite_administrativo_type, :|BARRIO|)))
			#Tabla barrio
			tabla_destino << carto.collection(:ds_carto_barrio)
			#Campo geom�trico a utilizar
			campo_boundary << :boundary_barrio
			#Nombre para el checkpoint
			para_checkpoint << _self.message(:barrios)
		_endif
		
		#Carga de los l�mites  a usar
		limites << rwo_set.new()
		#Para cada municipio dado
		_for int!mun _over int!municipios.fast_elements()
		_loop
			#B�squeda l�mites que tocan el �rea del municipio
			int!limites << tabla_limite.geometry_set(gis.world, _unset, campo_boundary).select(:overlaying, int!mun.boundary).rwo_set()
			limites.add_all(int!limites)
		_endloop
		
		_if limites.size > 0 _then
			#Creaci�n del checkpoint previo
			a_crear << _self.message(:antes_migrar,para_checkpoint)
			carto.checkpoint(a_crear)
			#Indicador de proceso culminado
			completado? << _false
			#Cambio modo escritura
			carto.switch(:write)
			_protect
				#Para cada objeto l�mite encontrado
				_for limite _over limites.fast_elements()
				_loop @limites
					#Generaci�n de consulta para elementos duplicados
					consulta_existentes << _if (tipo_elemento = :perimetro_urbano) _then
											>> predicate.eq(:codigo_ciudad, limite.codigo) _and predicate.eq(:nombre, limite.name)
										_else
											>> predicate.eq(:codigo_barrio, limite.codigo) _and predicate.eq(:nombre, limite.name.default(_self.message(:desconocido)))	
										_endif
					#Si el l�mite est� duplicado, se pasa al siguiente
					_if (tabla_destino.select(consulta_existentes).size > 0) _then
						write(_self.message(:elemento_existente, limite.codigo.write_string, limite.name.default("")))
						_continue @limites
					_endif				
				
					#Se toma el campo geom�trico adecuado
					int!boundary << limite.perform(campo_boundary) 
					#Se obtinen los municipios que interactuan con el l�mite actual
					municipios_intersectados << tabla_municipios.geometry_set(carto.world).select(:overlaying, int!boundary.bounds).rwo_set()
					
					#Selecci�n del municipio correcto
					municipio_a_usar << _if (municipios_intersectados.size > 1) _then
										#Para el caso de m�ltiples municipios
										municipio_mayor << {_unset, 0}
										_for int!municipio _over municipios_intersectados.fast_elements()
										_loop
											#Se interceptan los municipios con el l�mite para obtener el �rea
											_try
												_if (area_int << int!boundary.intersection(int!municipio.boundary)) _isnt _unset _then
													int!area << area_int.area
													#Si el �rea nueva es mayor que el �rea actual, se toma como actual
													_if (municipio_mayor[2] < int!area) _then
														municipio_mayor << {int!municipio, int!area}
													_endif
												_endif
											_when polygon_acp_invalid_area
												write(_self.message(:area_elemento_solapada, limite.id, limite.codigo.write_string, limite.name.default("")))
												_continue @limites
											_endtry
										_endloop
										>> municipio_mayor[1]
									_else
										#En el caso de un s�lo municipio
										>> municipios_intersectados.an_element()
									_endif
					
					#Generaci�n del registro en blanco de la tabla adecuada			
					registro << tabla_destino.new_detached_record()
					
					#Asignaci�n de valores en los campos dependiendo del tipo de objeto
					_if (tipo_elemento = :perimetro_urbano) _then
						registro.codigo_ciudad << limite.codigo
						registro.nombre << limite.name
					_else
						registro.codigo_barrio << limite.codigo
						registro.nombre << limite.name.default(_self.message(:desconocido))
					_endif

					#Inserci�n del registro
					registro_insertado << tabla_destino.insert(registro)
					#Mensaje de seguimiento
					_if (registro_insertado _is _unset) _then
						write(_self.message(:registro_rechazado, limite.id, limite.codigo.write_string, limite.name.default("")))
					_else
						#Asignaci�n de valores comunes
						registro_insertado.ds_carto_municipio << municipio_a_usar
						int!area << area.new(tabla_destino.field(:boundary).descriptor_for(tabla_destino), int!boundary)
						registro_insertado.set_geometry(int!area)
						registro_insertado.make_annotation()
						write(_self.message(:registro_creado, registro_insertado.id, limite.codigo.write_string, registro_insertado.nombre.default("")))
					_endif
				_endloop
				#Acometida de los cambios
				carto.commit()
				#Generaci�n del checkpoint posterior al transporte
				carto.checkpoint(_self.message(:despues_migrar, para_checkpoint))
				#Proceso completado
				completado? << _true
			_protection
				#En caso que ocurra un condici�n
				_if _not completado? _then
					#Se deshacen los cambios
					carto.rollback()
					#Se elimina el checkpoint creado originalmente
					carto.remove_checkpoint(a_crear)
				_endif
				#Se cambia el modo a solo lectura
				carto.switch(:readonly)
			_endprotect
		_else
			write(_self.message(:sin_elementos))
		_endif
		write(_self.message(:proceso_finalizado))
	_endif
_endmethod
$
	
_pragma(classify_level=basic, topic={transporte})
##**********************************************************************************************************************************
##	M�todo: validar_entradas
##	Descripci�n: realiza las validaciones sobre los par�metros de entrada de la ejecuci�n de un transporte de elementos 
##	Par�metros de entrada: 	tipo de elemento a trasnportar, como s�mbolo
##						arreglo con los ids de los municipios a utilizar
##   	Par�metros de salida: 	mensaje a presentar
##	Condiciones: ninguna
##
##**********************************************************************************************************************************
_private _method transportar_elementos.validar_entradas(tipo_elemento, municipios)
	#Bandera de validez
	valido? << _true
	#Para los elementos de tipo per�metro urbano
	_if tipo_elemento = :perimetro_urbano _then
		#Se valida que haya por lo menos un municipio
		_if municipios.size = 0 _then
			write(_self.message(:sin_municipios))
			valido? << _false
		_endif
	#Para los elementos de tipo barrio
	_elif tipo_elemento = :barrio _then
		#Se valida que exista solo un municipio
		_if (municipios.size <> 1) _then
			write(_self.message(:solo_un_municipio))
			valido? << _false
		_endif
	#No se permite ning�n otro tipo de elemento
	_else
		write(_self.message(:elemento_no_reconocido,tipo_elemento))
		valido? << _false
	_endif
	
	>> valido?
_endmethod

_pragma(classify_level=basic, topic={transporte})
##**********************************************************************************************************************************
##	M�todo: message
##	Descripci�n: manejo de mensajer�a a trav�s de archivos msg 
##	Par�metros de entrada: 	nombre del mensaje como un simbolo
##						opcionalmente, la lista de variables para complementar el mensaje
##   	Par�metros de salida: 	mensaje a presentar
##	Condiciones: ninguna
##
##**********************************************************************************************************************************
_method transportar_elementos.message(name, _gather args)
	#Carga del manejador de mensajes
	mh << message_handler.new(:transportar_elementos)
	>> mh.human_string(name, _unset ,_scatter args)
_endmethod
$

