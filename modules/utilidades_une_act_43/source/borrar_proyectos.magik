#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Migraci�n 4.1 a 4.2
##	Archivo: borrar_proyectos.magik
##	Contenido: Procedimiento para eliminaci�n de proyectos
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 01-07-2015
##	Historial de cambios:  17-12-2015
##	OJF 	01-07-2015	Primera versi�n
##	OJF	19-10-2015	Adecuaci�n a versi�n 4.3
##	OJF	17-12-2015	Cambio de validaci�n para el estatus hacia el esquema en lugar del proyecto
##
##**********************************************************************************************************************************
_package sw

##**********************************************************************************************************************************
##	Procedimiento: eliminar_proyectos 
##	Descripci�n: Procedimiento para eliminar proyectos seg�n los estados suministrados
##	Par�metros de entrada:  lista de estados como string
##    Par�metros de salida: Ninguno
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_global eliminar_proyectos << _proc(fecha_inicio, fecha_fin, _optional lista_usuarios, estados)
	#Valor por defecto para lista no suministrada
	int!lista_usuarios << rope.new_from(lista_usuarios.default(rope.new()))
	#Se ingresa el usuario root para evitar la eliminaci�n de toda alternativa que posea
	int!lista_usuarios.add("root")
	int!lista_usuarios.add("admin")
	
	#Valor por defecto para los estados
	int!estados << estados.default(rope.new())
	#Estados v�lidos
	estados_diseno << {"New Job", "Approved", "Approved (No Publish)", "As-built Updates", "Cancelled", "Completed", "Design Layout", "Post Pending", "Submitted for Approval"}

	#Formato de fechas
	_try 
		int!fecha_inicio << date.new_from_string(fecha_inicio,"#<day_of_month(2)>/#<month_number(2)>/#<year(4)>")
		int!fecha_fin << date.new_from_string(fecha_fin,"#<day_of_month(2)>/#<month_number(2)>/#<year(4)>")
	_when error
		#Error en el formato
		write("Error en el formato de las fechas de entrada")
		_return
	_endtry
	
	#Predicado a usar
	predicado << _unset
	
	#Para cada estado suministrado
	_for estado _over int!estados.fast_elements()
	_loop
		#Se valida si es un estado correcto
		_if (_not estados_diseno.includes_by_equality?(estado)) _then
			#Estado incorrecto, mensaje de error y retorno
			write("Estado suministrado no v�lido: ",estado)
			_return
		_endif
	_endloop
	
	#Cambio 17-12-2015 Cambio de la validaci�n del estatus hacia los esquemas en lugar de los proyectos
	#Si se indicaron estados y todos son correcto se prepara el predicado de join para los esquemas
	_if int!estados.size > 0 _then
		int!predicado << predicate.all(:schemes, predicate.in(:status, int!estados))
	_endif
	
	#Adici�n de las fechas al predicado
	predicado << predicate.ge(:planning_start, int!fecha_inicio) _and predicate.le(:planning_start, int!fecha_fin)
	predicado << _if int!predicado _is _unset _then >> predicado _else >> predicado _and int!predicado _endif
	#Fin del cambio 17-12-2015
	
	#Datasets a utilizar
	gis << view(:gis)
	dm << view(:design_admin)
	
	#actualizaci�n de los datasets
	gis.rollforward()
	dm.rollforward()
	
	#Cambio 19-10-2015 Inicializaci�n de swg_dsn_admin_engine en caso que no haya sido inicializada
	_if swg_dsn_admin_engine.view _is _unset _then
		swg_dsn_admin_engine.init()
	_endif
	#Fin del cambio 19-10-2015
	
	#Tabla a utilizar
	proyectos << dm.collection(:swg_dsn_project)
	#Dataset info a utilizar
	dsi << swg_dsn_admin_engine.dataset_infos.an_element()
	
	#Proyectos seg�n los estados suministrados
	proyectos_a_usar << proyectos.select(predicado)
	
	write("---Inicio de proceso de eliminaci�n de proyectos---")
	
	#Para cada proyecto encontrado
	_for proyecto _over proyectos_a_usar.fast_elements()
	_loop @principal
		#Nombre del proyecto
		nombre_proyecto << proyecto.name.shallow_copy()
		#Bandera para indicar errores con los esquemas o las alternativas
		errores? << _false
		#No se toma en cuenta el proyecto de Construction View
		_if (_not proyecto.is_build_plan?) _then
			#Pilas para el procesamiento de alternativas
			pila_esquemas << stack.new()
			pila_alternativas << stack.new()
			#Para cada esquema que se encuentre en el proyecto
			_for esquema _over proyecto.dsm_all_schemes().fast_elements()
			_loop
				#Se ubica la ruta de la alternativa asociada y se colocan el esquema y la ruta en sus respectivas pilas
				tmp!alt_path << swg_dsn_user_alternative_control.alt_path_from_scheme_path(dsi, esquema.scheme_path())
				pila_esquemas.add(esquema)
				pila_alternativas.add(tmp!alt_path)
			_endloop
			#Para cada esquema
			_loop
				#Si la pila esta vac�a se termina el ciclo
				_if (pila_esquemas.empty?) _then
					_leave
				_endif
				
				#Se toman el esquema, la ruta y el nombre del esquema
				esq << pila_esquemas.next()
				alt_path << pila_alternativas.next()
				nombre_esquema << esq.name.shallow_copy()

				desde_alt << ""
				_try 
					#Se ignoran los proyectos con usuarios indicados
					_if int!lista_usuarios.includes_by_equality?(esq.owner) _then
						errores? << _true
						_continue
					_endif
					#Eliminacion del esquema y la alternativa
					swg_dsn_admin_engine.delete_scheme(esq)
					_if gis.alternative_exists?(%|+alt_path.join_as_strings(%|)) _then
						#Se toma la ruta padre de la alternativa a borrar
						path_a_usar << alt_path.slice(1, alt_path.size-1)
						#Nombre de la alternativa
						nombre_alt << alt_path[alt_path.size]
						#Cambio a la alternativa padre, necesario para la eliminaci�n
						gis.go_to_alternative(%|+path_a_usar.join_as_strings(%|))
						gis.remove_alternative(nombre_alt)
						desde_alt << " alternativa: "+nombre_alt
					_endif
					write("Eliminado esquema: ",nombre_esquema," del proyecto: ",nombre_proyecto,desde_alt)
				_when swg_dsn_scheme_occupied
					#Si el esquema est� en uso, mensaje  de error y se continua con el siguiente esquema
					write("No se puede eliminar esquema: ",nombre_esquema," del proyecto: ",nombre_proyecto, " ya que se encuentra en uso")
					errores? << _true
					_continue				
				_when error
					#Escritura concurrente, se termina el proceso completo
					write("No se elimin� ", nombre_esquema," del proyecto: ",nombre_proyecto, ", por existir escritura m�ltiple")
					write("Se sale del proceso por escritura concurrente en el dataset Design Admin")
					_leave @principal
				_endtry				
			_endloop
			_try
				#Si no hubo errores  con los esquemas, se procede a eliminar el proyecto
				_if (_not errores?) _then
					swg_dsn_admin_engine.delete_project(proyecto, _true)
					write("Eliminado proyecto: ",nombre_proyecto)
				_else
					#Si hubo errores, no se elimina el proyecto
					write("No se elimin� proyecto: ",nombre_proyecto, " ya que posee esquemas en uso o de usuarios protegidos")
				_endif
			_when error
				#Escritura concurrente, se termina el proceso completo
				write("No se elimin� el proyecto: ",nombre_proyecto, ", por existir escritura m�ltiple")
				write("Se sale del proceso por escritura concurrente en el dataset Design Admin")
				_leave @principal
			_endtry
		_endif
	_endloop
	write("---- Proceso finalizado con �xito ---")
_endproc
