#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Migraci�n Smallworld 4.1 a 4.3
##	Archivo: georeferencia_service_provider.magik
##	Contenido: Service provider para los servicios de georeferenciaci�n
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 10-09-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw
$

_pragma(classify_level=basic, topic={services})
##Definici�n de la clase
def_slotted_exemplar(:georeferencia_service_provider,
{
},
:service_provider)
$

_pragma(classify_level=basic, topic={services})
## Servicios definidos por esta clase
georeferencia_service_provider.define_interface( :service,
		:methods, {
				:georeferenciar_direccion|()|
		})
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: georeferenciar_direccion 
##	Descripci�n: Servicio para la georeferenciaci�n de direcciones
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method georeferencia_service_provider.georeferenciar_direccion( a_service_call )
	
	#Ubicaci�n de los par�metros a utilizar
	parametros << _self.get_service_call_parameter(a_service_call, :|parametros|)

	#Obtenci�n de los par�metros de b�squeda para la direcci�n
	codigo_pais << parametros[:|codigoPais|]
	codigo_departamento << parametros[:|codigoDepto|]
	codigo_municipio << parametros[:|codigoMunicipio|]
	codigo_barrio << parametros[:|codigoBarrio|]
	cadena_normalizada << parametros[:|direccionNormalizada|]
	
	#Dataset a utilizar 
	une_st << _self.database.dataset(:une_st)
	
	#Actualizaci�n de dataset
	une_st.rollforward()
	
	#Tabla a utilizar
	tabla_det_direccion << une_st.collection(:detalle_direccion)
	
	#Cadena vac�a, se coloca por eficiencia
	vacio << ""
	
	#Inicializaci�n de objeto de respuesta
	respuesta << property_list.new()
	
	#Objeto que controla los mensajes de respuesta
	detalleRespuesta << property_list.new()
	detalleRespuesta[:|CodigoRespuesta|] << _self.message(:ko)
	detalleRespuesta[:|CodigoError|] << vacio
	detalleRespuesta[:|DescripcionError|] << vacio
	detalleRespuesta[:|MensajeError|] << vacio
	
	respuesta[:|DetalleRespuesta|] << detalleRespuesta
	
	#C�digo de direcci�n a retornar
	mgCodigoRespuesta << vacio
	
	#Bandera para indicar acometida
	commit? << _false
	
	_protect
		#Inicio de la transacci�n
		une_st.start_short_transaction()
		
		_try
		_with condicion
			#Se ubica el registro de direcci�n
			reg_detalle_direccion << tabla_det_direccion.select(predicate.eq(:codigo_pais, codigo_pais) _and
													predicate.eq(:codigo_departamento, codigo_departamento) _and
													predicate.eq(:codigo_municipio, codigo_municipio) _and
													predicate.eq(:codigo_barrio, codigo_barrio) _and
													predicate.eq(:cadena_normalizada, cadena_normalizada)).an_element()
			
			#Se toma el valor num�rico del estado
			estado_respuesta << _self.convertir_letra_a_numero(parametros[:|estadoRespuesta|].default(vacio))
			exito? << _true
			
			#Si existe el registro de direcci�n
			_if reg_detalle_direccion _isnt _unset _then
				#Se asigna el c�digo a regresar
				mgCodigoRespuesta << reg_detalle_direccion.codigo.write_string
				#Se toma el valor num�rico del estado del registro
				estado_geor_type << _self.convertir_letra_a_numero(reg_detalle_direccion.estado_geor_type.write_string)
				
				#Si existe mejora del estado
				_if estado_respuesta > estado_geor_type _then
					#Consulta de bloqueos para el registro
					(estado, usuario, identificador) << une_st.query_locked_record(reg_detalle_direccion)
				
					#Si el registro no esta bloqueado
					_if estado = :unknown _then
						#Bloqueo de registros
						conj_registros << {reg_detalle_direccion}
						une_st.lock_records(conj_registros,conj_registros)
						#Se actualizan los valores
						_self.asignar_valores(reg_detalle_direccion, parametros)
						#Se marca la acometida
						commit? << _true
					_else
						#Registro bloqueado
						detalleRespuesta[:|CodigoError|] << _self.message(:codigo_registro_bloquedado)
						detalleRespuesta[:|DescripcionError|] << _self.message(:desc_registro_bloquedado)
						detalleRespuesta[:|MensajeError|] << _self.message(:mens_registro_bloquedado)
						#Limpieza del c�digo
						mgCodigoRespuesta << vacio
						#Falla
						exito? << _false
					_endif
				_endif		
			_else
				#Se toma el nuevo c�digo
				mgCodigoRespuesta << une_st.uvas[:detalle_direccion].get()
				#Si el estado es uno de los esperados
				_if estado_respuesta > 0 _then
					#Registro vac�o
					reg_detalle_direccion << tabla_det_direccion.new_detached_record()
					#Campos claves y de b�squeda
					reg_detalle_direccion.codigo << mgCodigoRespuesta
					reg_detalle_direccion.codigo_pais << codigo_pais
					reg_detalle_direccion.codigo_departamento << codigo_departamento
					reg_detalle_direccion.codigo_municipio << codigo_municipio
					reg_detalle_direccion.codigo_barrio << codigo_barrio
					reg_detalle_direccion.cadena_normalizada << cadena_normalizada
					#Asignaci�n del resto de los campos
					_self.asignar_valores(reg_detalle_direccion, parametros)
					
					#Inserci�n del registro
					_if (reg_detalle_direccion << tabla_det_direccion.insert(reg_detalle_direccion) _isnt _unset) _then
						#Acometida de los cambios
						commit? << _true
					_else
						#Registro duplicado
						detalleRespuesta[:|CodigoError|] << _self.message(:codigo_registro_duplicado)
						detalleRespuesta[:|DescripcionError|] << _self.message(:desc_registro_duplicado, _unset, mgCodigoRespuesta)
						detalleRespuesta[:|MensajeError|] << _self.message(:mens_registro_duplicado, _unset, mgCodigoRespuesta)
						#Limpieza del c�digo
						mgCodigoRespuesta << vacio
						#Falla
						exito? << _false
					_endif
				_endif
			_endif	
			
			#En caso de �xito, se coloca mensaje ok
			_if exito? _then
				#Respuesta correcta
				detalleRespuesta[:|CodigoRespuesta|] << _self.message(:ok)
			_endif

		_when error
			#Error t�cnico
			detalleRespuesta[:|CodigoError|] << _self.message(:codigo_error_tecnico)
			detalleRespuesta[:|DescripcionError|] << _self.message(:error_tecnico)
			detalleRespuesta[:|MensajeError|] << condicion.report_string
		_endtry
		#Asignaci�n de la fecha
		detalleRespuesta[:|FechaRespuesta|] << property_list.new_with(:date, date_time.now())
	_protection
		#Cierre de la transacci�n con el resultado seg�n lo manejado
		une_st.end_short_transaction(commit?)
	_endprotect
	
	#Asignaci�n del c�digo de direcci�n
	respuesta[:|mgCodigoRespuesta|] << mgCodigoRespuesta.write_string
	
	#Respuesta del servicio
	_local response << a_service_call.response

	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|respuesta|, respuesta)
	
	>> response
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: asignar_valores 
##	Descripci�n: Asigna los valores desde una lista de propiedades a un registro de direcci�n
##	Par�metros de entrada:  el registro de direcci�n
##						la lista de propiedades
##    Par�metros de salida: ninguno
##	Condiciones: ninguna
##
##**********************************************************************************************************************************
_private _method georeferencia_service_provider.asignar_valores(reg_detalle_direccion, parametros)
	#Asignaci�n de valores
	reg_detalle_direccion.codigo_comuna	 << parametros[:|codigoLocalidad|].default(vacio)
	reg_detalle_direccion.nombre_comuna << parametros[:|nombreLocalidad|].default(vacio)
	reg_detalle_direccion.codigo_urbanizacion	<< parametros[:|codigoUrb|].default(vacio)
	reg_detalle_direccion.codigo_manzana << parametros[:|codigoManzana|].default(vacio)
	reg_detalle_direccion.codigo_predio << parametros[:|codigoPredio|].default(vacio)
	reg_detalle_direccion.es_rural << parametros[:|rural|].default(vacio)
	reg_detalle_direccion.numero_placa << parametros[:|placa|].default(vacio)
	reg_detalle_direccion.agregado_complemento << parametros[:|agregado|].default(vacio)
	reg_detalle_direccion.remanente << parametros[:|remanente|].default(vacio)
	reg_detalle_direccion.codigo_dir_proveedor << parametros[:|codigoDireccion|].default(vacio)
	reg_detalle_direccion.estrato_type << parametros[:|estrato|].as_integer().default(0)
	reg_detalle_direccion.Latitud << float.from_write_string(parametros[:|latitud|].default(0))
	reg_detalle_direccion.Longitud << float.from_write_string(parametros[:|longitud|].default(0))
	reg_detalle_direccion.estado_geor_type << parametros[:|estadoRespuesta|]
_endmethod

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: convertir_letra_a_numero 
##	Descripci�n: Convierte la letra de un estado de georeferenciaci�n a un n�mero
##	Par�metros de entrada:  la letra a convertir
##    Par�metros de salida: el n�mero asociado a la letra
##	Condiciones: ninguna
##
##**********************************************************************************************************************************
_private _method georeferencia_service_provider.convertir_letra_a_numero(letra)
	#Asignaci�n del n�mero
	respuesta << _if letra = "M" _then
					>> 10
				_elif letra = "N" _then
					>> 8
				_elif letra = "B" _then
					>> 6
				_elif letra = "Y" _then
					>> 4
				_else
					>> 0
				_endif
	
	#Retorno de valores
	>> respuesta
_endmethod

##	Manejo de mensajes
message_handler.new(:georeferencia_service_provider).add_use(:service_provider)
$