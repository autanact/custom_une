#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Gestor de Direcciones Excepcionadas
##	Archivo: gestion_tablas_gde_service_provider.magik
##	Contenido: Service provider para los servicios relacionados con el manejo de tablas en general
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 28-05-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw
$

_pragma(classify_level=basic, topic={services})
##Definici�n de la clase
def_slotted_exemplar(:gestion_tablas_gde_service_provider,
{
},
:service_provider)
$

##Constante que tiene el formato para las fechas a utilizar
gestion_tablas_gde_service_provider.define_shared_constant( :formato_anio,  {:date, :year, 4, %0}, _true) 
$

_pragma(classify_level=basic, topic={services})
## Servicios definidos por esta clase
gestion_tablas_gde_service_provider.define_interface( :service,
		:methods, {
			:actualizar_tablas_gde|()|,
			:consultar_tablas_gde|()|,
			:eliminar_tablas_gde|()|,
			:insertar_tablas_gde|()|,
			:obtener_objetos_urn|()|,
			:consultar_urn|()|
		})
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: actualizar_tablas_gde 
##	Descripci�n: Servicio para actualizaci�n de un registro de una tabla
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.actualizar_tablas_gde( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	nombreDataset << _self.get_service_call_parameter(a_service_call, :|nombreDataset|)
	nombreTabla << _self.get_service_call_parameter(a_service_call, :|nombreTabla|)
	nombresCamposAct << _self.get_service_call_parameter(a_service_call, :|nombresCamposAct|)
	valoresCamposAct << _self.get_service_call_parameter(a_service_call, :|valoresCamposAct|)
	nombreCampoClave << _self.get_service_call_parameter(a_service_call, :|nombreCampoClave|)
	valorCampoClave << _self.get_service_call_parameter(a_service_call, :|valorCampoClave|)
	
	#Mensajes de respuesta por defecto
	codigoRespuesta << _self.message(:ko)
	descripcionError << ""
	
	#Bandera para indicar si se debe proseguir con la actualizaci�n
	continuar? << _false
	
	#Ubicaci�n del dataset y la colecci�n indicada
	_if (int!dataset << _self.database.dataset(nombreDataset.as_symbol())) _isnt _unset _then
		_if (int!table << int!dataset.collection(nombreTabla.as_symbol())) _isnt _unset _then
			#Refrescamiento del dataset
			int!dataset.rollforward()
			#Nombre del campo como s�mbolo y objeto relacionado al campo	
			nombre_campo << nombreCampoClave.as_symbol()
			campo_fisico << int!table.field(nombre_campo)
			#Si se ubica el campo
			_if campo_fisico _isnt _unset _then
				_try 
					#Ubicaci�n del registro
					registro << int!table.select(predicate.eq(nombre_campo, campo_fisico.value_from_string(valorCampoClave))).an_element()
					#Si no existe el registro, se escribe mensaje de error, si existe se permite continuar
					_if registro _is _unset _then
						descripcionError << _self.message(:registro_no_encontrado)
					_else 
						continuar? << _true
					_endif
				_when error
					#Mensaje para campos con valor incorrecto
					descripcionError << _self.message(:valor_invalido, valorCampoClave, nombre_campo)
				_endtry
			_else
				#Mensaje de campo no existente
				descripcionError << _self.message(:campo_inexistente, nombre_campo)
			_endif
			
			#Si se permite continuar
			_if continuar? _then
				#Manejo de Short Transaction
				usa_sts? << int!dataset.mode = :write
				_if _not usa_sts? _then
					int!dataset.switch(:write)
				_endif

				registro_suelto << registro.detached()
				
				#Actualizaci�n de los campos
				(valido?, descripcionError) << _self.llenar_campos(registro_suelto, nombresCamposAct, valoresCamposAct)
				
				#Si la actualizaci�n fue correcta
				_if valido? _then
					int!table.insert_or_update(registro_suelto)
				
					#Acometida de los cambios y mensaje de �xito
					int!dataset.commit()
					codigoRespuesta << _self.message(:ok)
				_else
					#Si hubo errores se realiza deshacer
					int!dataset.rollback()
				_endif
				
				#Manejo de Short Transaction
				_if _not usa_sts? _then
					int!dataset.switch(:read_only)
				_endif
			_endif
		_else
			#Mensaje tabla no encontrada
			descripcionError << _self.message(:tabla_no_encontrada)
		_endif
	_else 
		#Mensaje dataset no encontrado
		descripcionError << _self.message(:dataset_no_encontrado)
	_endif
	
	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)

	>> response

_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: consultar_tablas_gde 
##	Descripci�n: Servicio para consultar registros de una tabla 
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.consultar_tablas_gde( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	nombreDataset << _self.get_service_call_parameter(a_service_call, :|nombreDataset|)
	nombreTabla << _self.get_service_call_parameter(a_service_call, :|nombreTabla|)
	nombresCamposConsulta << _self.get_service_call_parameter(a_service_call, :|nombresCamposConsulta|)
	valoresCamposConsulta << _self.get_service_call_parameter(a_service_call, :|valoresCamposConsulta|)
	nombreCamposResultado << _self.get_service_call_parameter(a_service_call, :|nombreCamposResultado|)

	#Se preparan los campos de retorno
	campos_retorno << _self.procesar_linea(nombreCamposResultado)

		#Ejecuci�n de la consulta
		(resultados, codigoRespuesta, descripcionError) << _self.int!ejecutar_query(nombreDataset, nombreTabla, nombresCamposConsulta, valoresCamposConsulta)
		
		#Si la consulta se ejecuta correctamente
		_if codigoRespuesta = _self.message(:ok) _then
			#Inicializaci�n de respuesta
			registros << "0"
			
			#Verificaci�n de consulta vac�a
			_if resultados.size > 0 _then
				#Se validan los campos de retorno
				(codigoRespuesta, descripcionError) << _self.validar_campos_salida(resultados.an_element().source_collection, campos_retorno)
				#Se contin�a si los campos son v�lidos
				_if codigoRespuesta = _self.message(:ok) _then	
					#Validaci�n de campos de respuesta
					#Total de registros
					nro_registro << 1
					#Buffer para la escritura de la salida
					int!buffer << internal_text_output_stream.new()
					#Para cada resultado obtenido
					_for registro _over resultados.fast_elements()
					_loop
						#Linea concerniente al registro
						linea << rope.new()
						#Para cada campo a regresar
						_for campo_devolver _over campos_retorno.fast_elements() 
						_loop
							#Ubicaci�n del nombre del campo
							campo_compuesto << campo_devolver.split_by(%.)
							simbolo_campo << campo_compuesto[1].as_symbol()
							
							#Si el campo es compuesto
							_if campo_compuesto.size > 1 _then
								#Se ubica la relaci�n
								tmp!objeto << registro.perform(simbolo_campo)
								#Si la relaci�n existe
								_if tmp!objeto _isnt _unset _then
									#Se agrega el valor del campo de la relaci�n
									linea.add(tmp!objeto.perform(campo_compuesto[2].as_symbol()).default("").write_string)
								_else
									#Valor por defecto
									linea.add("")
								_endif
							_else
								#Se agrega el valor del campo solicitado
								linea.add(registro.perform(simbolo_campo).default("").write_string)
							_endif
						_endloop
						#Verificaci�n de inicio del resultado
						_if nro_registro > 1 _then
							int!buffer.write(%|)
						_endif
						#Escritura de la l�nea
						int!buffer.write("[")
						int!buffer.write_list_with_separator(linea, %;)
						int!buffer.write("]")
						nro_registro +<< 1
					_endloop
					
					#Registros a retornar
					registros << int!buffer.string
					#Vaciado del buffer
					int!buffer.flush()
				_else
					registros << ""
				_endif
			_endif
		_endif

	
	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)
	response.set_parameter(:|registros|, registros)
	
	>> response

_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: eliminar_tablas_gde 
##	Descripci�n: Servicio para eliminar un registro de una tabla 
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.eliminar_tablas_gde( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	nombreDataset << _self.get_service_call_parameter(a_service_call, :|nombreDataset|)
	nombreTabla << _self.get_service_call_parameter(a_service_call, :|nombreTabla|)
	nombreCampoClave << _self.get_service_call_parameter(a_service_call, :|nombreCampoClave|)
	valorCampoClave << _self.get_service_call_parameter(a_service_call, :|valorCampoClave|)
	
	#Mensajes de respuesta por defecto
	codigoRespuesta << _self.message(:ko)
	descripcionError << ""
	
	#Bandera para indicar si se debe continuar con la eliminaci�n
	continuar? << _false
	
	#Ubicaci�n del dataset y la colecci�n indicada
	_if (int!dataset << _self.database.dataset(nombreDataset.as_symbol())) _isnt _unset _then
		#Actualizaci�n de los dataset
		int!dataset.rollforward()
		_if (int!table << int!dataset.collection(nombreTabla.as_symbol())) _isnt _unset _then
			#Actualizaci�n del dataset
			int!dataset.rollforward()
			
			#Nombre del campo y campo f�sico
			nombre_campo << nombreCampoClave.as_symbol()
			campo_fisico << int!table.field(nombre_campo)
			_if campo_fisico _isnt _unset _then
				_try 
					#Ubicaci�n del regisro
					registro << int!table.select(predicate.eq(nombre_campo, campo_fisico.value_from_string(valorCampoClave))).an_element()
					
					#Si no se consigue el registro, mensaje de error, sino continuar
					_if registro _is _unset _then
						descripcionError << _self.message(:registro_no_encontrado_eliminar)
					_else 
						continuar? << _true
					_endif
				_when error
					#Mensaje para valores errados
					descripcionError << _self.message(:valor_invalido, valorCampoClave, nombre_campo)
				_endtry
			_else
				#Mensaje campo inexistente
				descripcionError << _self.message(:campo_inexistente, nombre_campo)
			_endif
						
			#Si se permite continuar con la eliminaci�n
			_if continuar? _then
				#Manejo de Short Transaction
				usa_sts? << int!dataset.mode = :write
				_if _not usa_sts? _then
					int!dataset.switch(:write)
				_endif

				#Eliminaci�n del registro
				registro.delete()
				#Acometida de los cambios
				int!dataset.commit()
				#Mensaje de �xito
				codigoRespuesta << _self.message(:ok)
				
				#Manejo de Short Transaction
				_if _not usa_sts? _then
					int!dataset.switch(:read_only)
				_endif
			_endif
		_else	
			#Mensaje tabla no encontrada
			descripcionError << _self.message(:tabla_no_encontrada)
		_endif
	_else 
		#Mensaje dataset no encontrado
		descripcionError << _self.message(:dataset_no_encontrado)
	_endif
	
	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)

	>> response

_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: insertar_tablas_gde 
##	Descripci�n: Servicio para insertar un registro nuevo de una tabla 
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.insertar_tablas_gde( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	nombreDataset << _self.get_service_call_parameter(a_service_call, :|nombreDataset|)
	nombreTabla << _self.get_service_call_parameter(a_service_call, :|nombreTabla|)
	nombresCampos << _self.get_service_call_parameter(a_service_call, :|nombresCampos|)
	valoresCampos << _self.get_service_call_parameter(a_service_call, :|valoresCampos|)
	
	#Mensajes de respuesta por defecto
	codigoRespuesta << _self.message(:ko)
	descripcionError << ""
	
	#Ubicaci�n del dataset y la colecci�n indicada
	_if (int!dataset << _self.database.dataset(nombreDataset.as_symbol())) _isnt _unset _then
		#Actualizaci�n de los dataset
		int!dataset.rollforward()
		_if (int!table << int!dataset.collection(nombreTabla.as_symbol())) _isnt _unset _then
			#Manejo de Short Transaction
			usa_sts? << int!dataset.mode = :write
			_if _not usa_sts? _then
				int!dataset.switch(:write)
			_endif
			
			#Registro vac�o de la tabla indicada
			registro << int!table.new_detached_record()
			
			#Llenado de los campos
			(valido?, descripcionError) << _self.llenar_campos(registro, nombresCampos, valoresCampos)
			
			#Si el llenado fue correcto
			_if valido? _then
				#Inserci�n del registro
				resultado << int!table.insert(registro)
			
				#Si hubo problemas en la escritura, mensaje de error
				_if resultado _is _unset _then
					descripcionError << _self.message(:error_escritura)
				_else
					#Acometida de los cambios
					int!dataset.commit()
					#Mensaje de �xito
					codigoRespuesta << _self.message(:ok)
					
					#Obtenci�n de claves
					claves << int!table.key_fields
					
					#Env�o de la clave
					descripcionError << resultado.perform(claves[1].name).write_string
				_endif
			_endif
			
			#Manejo de Short Transaction
			_if _not usa_sts? _then
				int!dataset.switch(:read_only)
			_endif
		_else
			#Mensaje tabla no encontrada
			descripcionError << _self.message(:tabla_no_encontrada)
		_endif
	_else 
		#Mensaje dataset no encontrado
		descripcionError << _self.message(:dataset_no_encontrado)
	_endif
	
	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)

	>> response

_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: consultar_urn 
##	Descripci�n: Servicio para realizar una consulta que retorna urns
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.consultar_urn( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	nombreTabla << _self.get_service_call_parameter(a_service_call, :|nombreTabla|)
	nombreDataset << _self.get_service_call_parameter(a_service_call, :|nombreDataset|)
	nombresCamposConsulta << _self.get_service_call_parameter(a_service_call, :|nombresCamposConsulta|)
	valoresCamposConsulta << _self.get_service_call_parameter(a_service_call, :|valoresCamposConsulta|)
	
	#Ejecuci�n de la consulta
	(registros, codigoRespuesta, descripcionError) << _self.int!ejecutar_query(nombreDataset, nombreTabla, nombresCamposConsulta, valoresCamposConsulta)
	
	#inicializaci�n de valores a retornar
	resultados << rope.new()
	total << 0
	
	#Si la consulta fue satisfactoria
	_if codigoRespuesta = _self.message(:ok) _then
		#Para cada registro encontrado se toma el urn
		_for registro _over registros.fast_elements()
		_loop
			int!urn << _self.obtener_desde_objeto(registro)
			resultados.add(int!urn)
		_endloop
		#Total de registros
		total << registros.size
	_endif

	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|resultados|, resultados)
	response.set_parameter(:|totalResultados|, total.write_string)
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)	
	
	>> response
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: obtener_objetos_urn 
##	Descripci�n: Servicio para retorna lso campos deseados de una lista de urns
##	Par�metros de entrada:  el objeto llamada al servicio
##    Par�metros de salida: el objeto respuesta del servicio
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_method gestion_tablas_gde_service_provider.obtener_objetos_urn( a_service_call )
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Ubicaci�n de los par�metros a utilizar
	urns << _self.get_service_call_parameter(a_service_call, :|urns|)
	nombreCamposResultado << _self.get_service_call_parameter(a_service_call, :|nombreCamposResultado|)
	
	#Ajuste de campos a retornar
	campos_retorno << _self.procesar_linea(nombreCamposResultado)
	
	#Inicializaci�n de valores de respuesta
	resultados << rope.new()
	
	#Si se tienen URNS
	_if (urns.size > 0) _then
		#Se toma el primero
		##int!obj << urn_manager.get_object_from_urn(urns[1])
		int!obj << _self.obtener_desde_urn(urns[1])
		#Se validan los campos solicitados sobre la tabla origen del objeto
		(codigoRespuesta, descripcionError) << _self.validar_campos_salida(int!obj.source_collection, campos_retorno)
		#Si los campos est�n correctos
		_if codigoRespuesta = _self.message(:ok) _then
			#Se ubica el objeto para cada urn
			_for int!urn _over urns.fast_elements()
			_loop
				##int!obj << urn_manager.get_object_from_urn(int!urn)
				int!obj << _self.obtener_desde_urn(int!urn)
				_if(int!obj _is _unset) _then
					write(_self.message(:urn_no_encontrado,int!urn))
					_continue
				_endif
				#Propiedades a colectar
				elemento << property_list.new()
				#Para cada campo a regresar
				_for campo_devolver _over campos_retorno.fast_elements() 
				_loop
					#Ubicaci�n del nombre del campo
					campo_compuesto << campo_devolver.split_by(%.)
					simbolo_campo << campo_compuesto[1].as_symbol()
					
					#Si el campo es compuesto
					_if campo_compuesto.size > 1 _then
						#Se ubica la relaci�n
						tmp!objeto << int!obj.perform(simbolo_campo)
						#Ajuste del valor adecuado
						elemento[campo_compuesto.as_symbol()] << _if tmp!objeto _isnt _unset _then
																#Se agrega el valor del campo de la relaci�n
																>> tmp!objeto.perform(campo_compuesto[2].as_symbol()).default("").write_string
															_else
																#Valor por defecto							
																>> ""	
															_endif
					_else
						#Se agrega el valor del campo solicitado
						elemento[simbolo_campo] << int!obj.perform(simbolo_campo).default("").write_string
					_endif
				_endloop
				#Se agrega a la respuesta
				resultados.add(elemento)
			_endloop
		_endif
	_else
		#Mensaje urns no enviados
		codigoRespuesta << _self.message(:ko)
		descripcionError << _self.message(:urns_no_especificados)
	_endif
	
	#Respuesta del servicio
	_local response << a_service_call.response
	
	#Asignaci�n de c�digos de respuesta
	response.set_parameter(:|resultados|, resultados)
	response.set_parameter(:|codigoRespuesta|, codigoRespuesta)
	response.set_parameter(:|descripcionError|, descripcionError)	
	
	>> response	

_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: llenar_campos 
##	Descripci�n: llena los valores en un registro a partir de una lista de campos y de valores 
##	Par�metros de entrada:  el registro a llenar
##						la lista con el nombre de los campos
##						la lista con los valores a usar
##    Par�metros de salida: indicador de si el llenado fue realizado de forma correcta
##					   mensaje de error en caso de falla
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.llenar_campos(registro, nombresCampos, valoresCampos)
	#Configuracion de la fecha
	_dynamic !date_time_environ!
	!date_time_environ!.date_format[5] << _self.formato_anio
	
	#Valores de salida por defecto
	valido? << _true
	descripcionError << ""
	
	#Preparaci�n de campos y valores
	campos << nombresCampos.split_by(%|)
	valores << valoresCampos.split_by(%|)
	#Colecci�n a usar
	int!table << registro.source_collection

	#Para cada campo suministrado
	_for indice _over 1.upto(campos.size) 
	_loop
		#Nombre de campo como s�mbolo y campo f�sico
		nombre_campo << campos[indice].as_symbol()
		campo_fisico << int!table.field(nombre_campo)
		#Si existe el campo
		_if campo_fisico _isnt _unset _then
			_try 
				#Asignaci�n de valor a campo
				registro.perform(nombre_campo.with_chevron, campo_fisico.value_from_string(valores[indice]))
			_when db_thing_readonly
				#Error de valores incorrectos, se termina la ejecuci�n
				valido? << _false
				descripcionError << _self.message(:cambio_campo_clave, nombre_campo)	
				_leave
			_when error
				#Error de valores incorrectos, se termina la ejecuci�n
				valido? << _false
				descripcionError << _self.message(:valor_invalido, valores[indice], nombre_campo)
				_leave
			_endtry
		_else
			#Error de campo no existente
			valido? << _false
			descripcionError << _self.message(:campo_inexistente, nombre_campo)
			_leave
		_endif
	_endloop
	
	_return (valido?, descripcionError)
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: procesar_linea 
##	Descripci�n: realiza formato a la l�nea de valores recibida
##	Par�metros de entrada:  la l�nea a formatear
##    Par�metros de salida: secuencia con los valores ya formateados
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.procesar_linea(linea)
	#Secuencia a devolver
	retorno << rope.new()
	
	#Se procesa solo si existe la linea
	_if linea _isnt _unset _then
		#Preparaci�n de los valores contenidos en la l�nea
		valores << linea.split_by(%|)
		
		#Para cada valor obtenido
		_for valor _over valores.fast_elements()
		_loop
			#Indicador de rango
			_if valor[1] = %[ 
			_then
				retorno.add(valor.slice(2,(valor.size)-1).split_by(%;))
			#Indicador de multi valores
			_elif valor[1] = %{ _then
				retorno.add(rope.new_from(valor.slice(2,(valor.size)-1).split_by(%;)))
			_else
				#Valor simple
				retorno.add(valor)
			_endif
		_endloop
	_endif
	
	>> retorno
	
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: preparar_consulta_plana 
##	Descripci�n: prepara un predicado seg�n la informaci�n recibida
##	Par�metros de entrada:  la tabla sobre la cual se va a preparar el predicado
##						el campo sobre el que se va a preparar el predicado
##						el valor a utilizar
##    Par�metros de salida: el predicado generado
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.preparar_consulta_plana(int!table, campo, valor)
	#Nombre del campo como s�mbolo
	simbolo_campo << campo.as_symbol()
	int!predicado << _unset

	#Si existe el campo
	_if (campo_fisico << int!table.field(simbolo_campo)) _isnt _unset _then
		#Si el valor a utiliza es un rango 
		_if valor.is_class_of?(simple_vector) _then
			#Construccion de predicados mayor o igual y menor o igual
			int!predicado << predicate.ge(simbolo_campo, campo_fisico.value_from_string(valor[1])) _and
						  predicate.le(simbolo_campo, campo_fisico.value_from_string(valor[2]))
		#Si el valor a utilizar es de m�ltiples valores
		_elif valor.is_class_of?(rope) _then
			#Para cada valor 
			_for int!valor _over valor.fast_elements()
			_loop
				#Predicado de igualdad
				tmp!predicado << predicate.eq(simbolo_campo, campo_fisico.value_from_string(int!valor))
				#Construcci�n del predicado o
				_if int!predicado _is _unset _then
					int!predicado << tmp!predicado
				_else
					int!predicado << int!predicado _or tmp!predicado
				_endif
			_endloop
		_else
			#Predicado de igualdad para campos simples
			valor_a_usar << _if valor = "_unset" _then >> _unset _else >> campo_fisico.value_from_string(valor) _endif
			int!predicado << predicate.eq(simbolo_campo, valor_a_usar)
		_endif
	_endif
	
	>> int!predicado
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: int!ejecutar_query 
##	Descripci�n: realiza la ejecuci�n de un query
##	Par�metros de entrada:  el nombre del dataset donde se encuentra la tabla
##						el nombre de la tabla
##						lista de los campos sobre los cuales se va a realizar la consulta
##						lista de valores para los campos a consultar
##    Par�metros de salida:  resultado de la consulta
##					    codigo de respuesta OK o KO
##					    la descripci�n en caso de error
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.int!ejecutar_query(nombreDataset, nombreTabla, nombresCamposConsulta, valoresCamposConsulta)
	#Mensajes de respuesta por defecto
	codigoRespuesta << _self.message(:ko)
	descripcionError << ""
	registros << rope.new()

	#Ubicaci�n del dataset y la colecci�n indicada
	_if (int!dataset << _self.database.dataset(nombreDataset.as_symbol())) _isnt _unset _then
		#Actualizaci�n de los dataset
		int!dataset.rollforward()
		_if (int!table << int!dataset.collection(nombreTabla.as_symbol())) _isnt _unset _then
			#Ajuste de los valores recibidos
			campos_consulta << _self.procesar_linea(nombresCamposConsulta)
			valores_consulta << _self.procesar_linea(valoresCamposConsulta)
			
			#Predicado final
			predicado << _unset
			
			_if campos_consulta.size > 0 _then
				#Para cada campo de la consulta
				_for indice _over 1.upto(campos_consulta.size)
				_loop
					#Obtenci�n del campo y el valor a usar
					campo << campos_consulta[indice]
					valor << valores_consulta[indice]
					
					#Predicado para el campo
					int!predicado << _unset
					
					#Si el campo es de la forma (relacion.campo o relacion.size)
					_if (campo_compuesto << campo.split_by(%.)).size > 1  _then
						#Nombre de la relacion y campo que lo acompa�a
						simbolo_campo << campo_compuesto[1].as_symbol()
						acompanante << campo_compuesto[2]
						
						#Si existe el campo de join
						_if (campo_join << int!table.field(simbolo_campo)) _isnt _unset _then
							#Si el acompa�ante es size se prepara consulta de conteo
							_if acompanante = _self.message(:size) _then
								_if (campo_join.join_type _is :db_set_join) _then 
									_if (valor_entero << valor.as_integer()) _is _unset _then
										descripcionError << _self.message(:error_valor, valor)
										_leave
									_endif 
									int!predicado << predicate.count(simbolo_campo, predicate, :|=|, valor_entero)
								_else
									#Campo errado, se termina la ejecuci�n
									descripcionError << _self.message(:campo_no_size, simbolo_campo.write_string)
									_leave
								_endif
							_else
								#Nombre del campo acompa�ante
								simbolo_acompanante << acompanante.as_symbol()
								#Tabla destino de la relaci�n
								tabla_destino << int!dataset.collection(campo_join.result_table_name)
								_try
									#Preparar la consulta plana para los valores solicitados
									tmp!predicado << _self.preparar_consulta_plana(tabla_destino, simbolo_acompanante, valor)
								_when error
									#Valores errados, se termina la ejecuci�n
									descripcionError << _self.message(:error_valor, valor)
									_leave
								_endtry
								
								#Si el predicado plano es correcto, se prepara la consulta enlazada
								_if tmp!predicado _isnt _unset _then
									#Si la relaci�n es 1:1 se usa predicado de navegaci�n, si es de m�ltiples valores se usa el predicado cualquiera
									int!predicado << _if (campo_join.join_type _is :follow) _then								
													  >> predicate.navigate({simbolo_campo}, tmp!predicado)
												  _elif (campo_join.join_type _is :db_set_join) _then								
													  >> predicate.any(simbolo_campo, tmp!predicado)
												  _endif
								_else
									#Error de campo
									descripcionError << _self.message(:error_campo, campo)
									_leave
								_endif
							_endif
						_else
							#Error de campo
							descripcionError << _self.message(:error_campo, campo)
							_leave
						_endif
					_else
						_try
							#Se prepara el predicado para el campo
							int!predicado << _self.preparar_consulta_plana(int!table, campo, valor)
						_when error
							#Valores errados, se termina la ejecuci�n
							descripcionError << _self.message(:error_valor, valor)
							_leave
						_endtry
					_endif
					
					#Si el predicado interno no pudo construirse,  se termina la ejecuci�n
					_if int!predicado _is _unset _then
						_leave
					_endif
					
					#Verificaci�n para cuando el predicado general es vac�o de inicio
					_if predicado _is _unset _then
						predicado << int!predicado
					_else
						predicado << predicado _and int!predicado
					_endif
				_endloop
			_endif
			
			#Ejecucion de la consulta
			_if descripcionError = "" _then
				registros << int!table.select(predicado)
				codigoRespuesta << _self.message(:ok)
			_endif
		_else
			#Mensaje tabla no encontrada
			descripcionError << _self.message(:tabla_no_encontrada)
		_endif
	_else 
		#Mensaje dataset no encontrado
		descripcionError << _self.message(:dataset_no_encontrado)
	_endif
	
	>> registros, codigoRespuesta, descripcionError
_endmethod
$

_pragma(classify_level=advanced, topic={services})
##**********************************************************************************************************************************
##	M�todo: validar_campos_salida 
##	Descripci�n: valida los campos a solicitar sobre una colecci�n
##	Par�metros de entrada:  la tabla sobre la cual se va a realizar la validaci�n
##						las listas de los campos a validar
##    Par�metros de salida: codigo de respuesta OK o KO
##					    la descripci�n en caso de error
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.validar_campos_salida(int!table, campos_retorno)
	#Inicializaci�n de respuestas
	codigoRespuesta << _self.message(:ok) 
	descripcionError << ""
	#Dataset a usar
	int!dataset <<  int!table.dataset
	
	#Para cada campo retorno, se verifica que sean correctos
	_for campo_retorno _over campos_retorno.fast_elements() 
	_loop
		#Se obtiene el nombre el campo y el campo real
		campo_compuesto << campo_retorno.split_by(%.)
		simbolo_campo << campo_compuesto[1].as_symbol()
		campo_real << int!table.field(simbolo_campo)
		
		#Si existe un problema  con el campo, se env�a mensaje de error
		_if campo_real _is _unset _orif 
				(campo_compuesto.size > 1 _andif int!dataset.collection(campo_real.result_table_name).field(campo_compuesto[2].as_symbol()) _is _unset) _then
			codigoRespuesta << _self.message(:ko) 
			descripcionError << _self.message(:error_campo, simbolo_campo)
			_leave
		_endif								
	_endloop 
	
	>> codigoRespuesta, descripcionError
_endmethod
$

##**********************************************************************************************************************************
##	M�todo: obtener_desde_urn 
##	Descripci�n: obtiene un objeto desde su urn
##	Par�metros de entrada:  el urn del objeto
##    Par�metros de salida: el objeto  solicitado
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.obtener_desde_urn(int!urn)
	#Decodificaci�n del urn

	data << int!urn.split_by(%�)

	dataset_name << data[1].as_symbol()
	collection_name << data[2].as_symbol()
	key << data[3]
	
	#Obtenci�n de la colecci�n
	int!col << _self.database.dataset(dataset_name).collection(collection_name)
	campos_claves << int!col.key_fields
	
	#Asignaci�n de campo correcto
	valor << campos_claves[1].value_from_string(key)

	#Consulta
	>> int!col.at(valor)
_endmethod
$

##**********************************************************************************************************************************
##	M�todo: obtener_desde_objeto 
##	Descripci�n: obtiene el urn de un objeto
##	Par�metros de entrada:  el objeto
##    Par�metros de salida: el urn  solicitado
##	Condiciones: Ninguna
##
##**********************************************************************************************************************************
_private _method gestion_tablas_gde_service_provider.obtener_desde_objeto(objeto)
	#Colecci�n y dataset a utilizar
	coleccion <<  objeto.source_collection
	ds << coleccion.dataset
	#Actualizaci�n del dataset
	ds.rollforward()
	
	#Obtenci�n de claves
	claves << coleccion.key_fields
	
	#Respuesta
	>> ds.original_dataset_name.write_string+%�+coleccion.name.write_string+%�+objeto.perform(claves[1].name).write_string
_endmethod
$

##	Manejo de mensajes
message_handler.new(:gestion_tablas_gde_service_provider).add_use(:service_provider)
$