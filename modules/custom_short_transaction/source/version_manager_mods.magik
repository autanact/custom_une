#% text_encoding = iso8859_1
##**********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto:  Migraci�n de Smallworld 4.1 a 4.3
##	Archivo: version_manager_mods.magik
##	Contenido: Ajustes para soporte de short transaction 
##	Autor: Olivier Franco
##	Fecha �ltima modificaci�n: 19-08-2015
##	Historial de cambios: Primera versi�n
##
##**********************************************************************************************************************************
_package sw

_pragma(classify_level=restricted, topic={database_version_management})
##**********************************************************************************************************************************
##	M�todo: action!set_mode()
##	Descripci�n: M�todo sobrecargado ajustar el modo a short transaction donde aplique
##	Par�metros de entrada:  Referirse a la documentaci�n de clases de CORE para version_manager
##    Par�metros de salida: Referirse a la documentaci�n de clases de CORE para version_manager
##	Condiciones: Referirse a la documentaci�n de clases de CORE para version_manager
##
##**********************************************************************************************************************************
_method version_manager.action!set_mode( writable? )
	##
	## Switch mode toggle. If we were a writer, simply switch to
	## reader without formality. If we were a reader, and at a
	## checkpoint, see if that is the version the man really wants
	## to update.
	##

	_protect
		_if (chk_as_w? << .view.checkpoint _isnt _unset)
		_then 
			_if _self.checkpoint_allowed_writable?( .view.checkpoint )
			_then
				# Checkpoint is writable - get dataset dependent prompt for
				# choice on whether to make the checkpoint the disk version,
				# update the current disk version or cancel
				chk_as_w? << _self.top_frame.alert( _self.message( _self.dssm( :switch_at_checkpoint? ),
										   .view.checkpoint ),
								    { _self.message( :rollforward_to_current ),
								      _self.message( :update_from_checkpoint ),
								      _self.message( :cancel ) },
								    { _false, _true, _unset},
								    _unset )
			_else
				# Checkpoint is readonly - get dataset dependent prompt for
				# choice on whether to update the disk version or cancel
				
				mess_id << _self.dssm( :switch_at_readonly_checkpoint? )
				
				chk_as_w? << _self.top_frame.alert( _self.message( _self.dssm( :switch_at_readonly_checkpoint? ),
										   .view.checkpoint ),
								    { _self.message( :rollforward_to_current ),
								      _self.message( :cancel )},
								    { _false, _unset },
								    _unset )
			_endif
			
			_if chk_as_w? _is _unset
			_then
				!abort!()
			_endif
		_endif 

		_if chk_as_w?
		_then 
			_self.go_to_checkpoint_as_writer( .view.checkpoint, _true )
		_else
			#Inicio del cambio Autana CT para manejo de Short Transaction
			int!modo << _if .view.has_st_connection_spec? _then >> :short_transaction _else >> :write _endif
			
			_self.perform_on_view( { :pre_status, :switching_mode,
						 :post_true_message, :view_changed,
						 :must_be_writable?, writable? _andif .view.mode _is :readonly
					       },
					       :switch|()|,
						   #Cambiado para manejo del Short Transaction
						   #_if writable? _then >> :write _else >> :readonly _endif )
					       _if writable? _then >> int!modo _else >> :readonly _endif )
			#Fin del cambio
		_endif

	_protection
		# the database mode toggle may be wrong after error: forcibly reset
		_self.action( :mode ).set_value ( .view.writable?, _false )
		_self.databus_make_data_available( :mode, .view )
	_endprotect 
	
_endmethod
$